import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib

mainloop = None

class Calculator(dbus.service.Object):
    def __init__(self, bus):
        self.path = "/com/example/calculator"
        dbus.service.Object.__init__(self, bus, self.path)

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
bus = dbus.SystemBus()
calc = Calculator(bus)

mainloop = GLib.MainLoop()
print("Waiting for some calculations to do...")
mainloop.run()